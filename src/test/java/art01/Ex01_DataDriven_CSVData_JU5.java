package art01;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;


import java.io.IOException;

import java.util.concurrent.TimeUnit;

public class Ex01_DataDriven_CSVData_JU5 {
	WebDriver driver;

	@BeforeEach
	public void beforeTest() {

		System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
	}



	public static String[][] dp() throws IOException {
		/*
		 * return new String[][] {

			new String[] { "https://www.google.com/", "Google" },
			new String[] { "https://www.imdb.com/", "IMDb:" },
			new String[] { "https://www.wikipedia.org/", "Wikipedia" }
		};
		*/
		String csvDataFilePath = "src\\test\\resources\\data\\urls_titles_data.csv";
		String[][] dataFromCSV = utils.CSVDataReaders.getCSVData(csvDataFilePath,',');
		return dataFromCSV;
	}

	@MethodSource("dp")
	@ParameterizedTest
	public void testMethod(String v1, String v2) {
		//System.out.println(v1+" ---  "+v2);
		String url = v1;
		String expectedTitle = v2;

		driver.get(url);
		String title = driver.getTitle();

		if (title.contains(expectedTitle)) {
			System.out.println("Expected Title " + expectedTitle + 
					"  contained in Actual Title  =" + title);
		} else {
			System.out.println("Expected Title " + expectedTitle + 
					"  NOT contained in Actual Title  =" + title);
		}
	}

	@AfterEach
	public void afterTest() {

		driver.quit();
	}

}
