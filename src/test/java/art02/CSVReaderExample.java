package art02;

public class CSVReaderExample {

	public static void main(String[] args) {
		String csvFile = "src\\test\\resources\\data\\Cricket_Score.csv";
		String[][] dataFromCSV = utils.CSVDataReaders.getCSVData(csvFile,':');

		for(String[] row : dataFromCSV) {

			for(String field : row) {
				System.out.print(field + "\t");
			}
			System.out.println();
		}
	}
}
